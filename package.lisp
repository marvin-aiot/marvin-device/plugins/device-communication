(uiop:define-package #:device-communication
  (:documentation "")
  (:use #:common-lisp)
  (:export #:device-communication-plugin
	   #:device
	   #:device-id
	   #:device-sn
	   #:device-message
	   #:device-message-id
	   #:device-service
	   #:device-service-id))

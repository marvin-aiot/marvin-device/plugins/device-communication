(in-package :device-communication)

(defclass Event-561 (communication-protocol:Event)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :reader communication-protocol:message-id
				      :initform 561
				      :allocation :class)
   (humidity :reader humidity
	     :type (unsigned-byte 8)
	     :initarg :humidity)))


(defmethod communication-protocol:populate-slots-from-frame ((message Event-561) frame)
  (setf (slot-value message 'humidity) (get-value-from-message frame)))



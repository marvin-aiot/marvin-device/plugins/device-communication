(in-package :device-communication)

(defclass Event-560 (communication-protocol:Event)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :reader communication-protocol:message-id
				      :initform 560
				      :allocation :class)
   (temperature :reader temperature
		:type (unsigned-byte 16)
		:initarg :temperature)))


(defmethod communication-protocol:populate-slots-from-frame ((message Event-560) frame)
  (setf (slot-value message 'temperature) (get-value-from-message frame)))



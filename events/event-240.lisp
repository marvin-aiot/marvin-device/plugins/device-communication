(in-package :device-communication)

(defclass Event-240 (communication-protocol:Event)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :reader communication-protocol:message-id
				      :initform 240
				      :allocation :class)
   (state :type boolean
	  :reader state
	  :initform nil
	  :initarg :state)))

(defclass Event-241 (Event-240)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :reader communication-protocol:message-id
				      :initform 241
				      :allocation :class)))

(defclass Event-242 (Event-240)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :reader communication-protocol:message-id
				      :initform 242
				      :allocation :class)))

(defclass Event-243 (Event-240)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :reader communication-protocol:message-id
				      :initform 243
				      :allocation :class)))

(defclass Event-244 (Event-240)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :reader communication-protocol:message-id
				      :initform 244
				      :allocation :class)))

(defclass Event-245 (Event-240)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :reader communication-protocol:message-id
				      :initform 245
				      :allocation :class)))

(defclass Event-246 (Event-240)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :reader communication-protocol:message-id
				      :initform 246
				      :allocation :class)))

(defclass Event-247 (Event-240)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :reader communication-protocol:message-id
				      :initform 247
				      :allocation :class)))

(defmethod communication-protocol:populate-slots-from-frame ((message Event-240) frame)
  (setf (slot-value message 'state) (get-value-from-message frame)))



(in-package :device-communication)

(defclass Event-547 (communication-protocol:Event)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :accessor communication-protocol:message-id
				      :initform 547
				      :allocation :class)
   (state :reader state
	  :type boolean
	  :initarg :state)))

(defclass Event-548 (Event-547)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :accessor communication-protocol:message-id
				      :initform 548
				      :allocation :class)))

(defclass Event-549 (Event-547)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :accessor communication-protocol:message-id
				      :initform 549
				      :allocation :class)))

(defclass Event-550 (Event-547)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :accessor communication-protocol:message-id
				      :initform 550
				      :allocation :class)))

(defclass Event-551 (Event-547)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :accessor communication-protocol:message-id
				      :initform 551
				      :allocation :class)))

(defclass Event-552 (Event-547)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :accessor communication-protocol:message-id
				      :initform 552
				      :allocation :class)))

(defclass Event-553 (Event-547)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :accessor communication-protocol:message-id
				      :initform 553
				      :allocation :class)))

(defclass Event-554 (Event-547)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :accessor communication-protocol:message-id
				      :initform 554
				      :allocation :class)))

(defmethod communication-protocol:populate-slots-from-frame ((message Event-547) frame)
  (setf (slot-value message 'state) (get-value-from-message frame)))



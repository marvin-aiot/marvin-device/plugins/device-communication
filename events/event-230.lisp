(in-package :device-communication)

(defclass Event-230 (communication-protocol:Event)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :reader communication-protocol:message-id
				      :initform 230
				      :allocation :class)
   (movement :type boolean
	     :reader movement
	     :initform nil
	     :initarg :movement)))


(defclass Event-231 (Event-230)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :reader communication-protocol:message-id
				      :initform 231
				      :allocation :class)))

(defclass Event-232 (Event-230)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :reader communication-protocol:message-id
				      :initform 232
				      :allocation :class)))

(defclass Event-233 (Event-230)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :reader communication-protocol:message-id
				      :initform 233
				      :allocation :class)))

(defclass Event-234 (Event-230)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :reader communication-protocol:message-id
				      :initform 234
				      :allocation :class)))

(defclass Event-235 (Event-230)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :reader communication-protocol:message-id
				      :initform 235
				      :allocation :class)))

(defclass Event-236 (Event-230)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :reader communication-protocol:message-id
				      :initform 236
				      :allocation :class)))

(defclass Event-237 (Event-230)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :reader communication-protocol:message-id
				      :initform 237
				      :allocation :class)))

(defmethod communication-protocol:populate-slots-from-frame ((message Event-230) frame)
  (setf (slot-value message 'movement) (get-value-from-message frame)))



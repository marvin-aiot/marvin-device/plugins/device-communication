(in-package :device-communication)

(defclass Event-562 (communication-protocol:Event)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :reader communication-protocol:message-id
				      :initform 562
				      :allocation :class)
   (luminosity :reader luminosity
	       :type float
	       :initarg :luminosity)))


(defmethod communication-protocol:populate-slots-from-frame ((message Event-562) frame)
  (setf (slot-value message 'luminosity) (get-value-from-message frame)))



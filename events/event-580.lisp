(in-package :device-communication)

(defclass Event-580 (communication-protocol:Event)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :reader communication-protocol:message-id
				      :initform 580
				      :allocation :class)
   (contact :reader contact
	    :type boolean
	    :initform nil
	    :initarg :contact)))

(defclass Event-581 (Event-580)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :reader communication-protocol:message-id
				      :initform 581
				      :allocation :class)))

(defclass Event-582 (Event-580)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :reader communication-protocol:message-id
				      :initform 582
				      :allocation :class)))

(defclass Event-583 (Event-580)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :reader communication-protocol:message-id
				      :initform 583
				      :allocation :class)))

(defclass Event-584 (Event-580)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :reader communication-protocol:message-id
				      :initform 584
				      :allocation :class)))

(defclass Event-585 (Event-580)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :reader communication-protocol:message-id
				      :initform 585
				      :allocation :class)))

(defclass Event-586 (Event-580)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :reader communication-protocol:message-id
				      :initform 586
				      :allocation :class)))

(defclass Event-587 (Event-580)
  ((communication-protocol:message-id :type (unsigned-byte 10)
				      :reader communication-protocol:message-id
				      :initform 587
				      :allocation :class)))


(defmethod communication-protocol:populate-slots-from-frame ((message Event-580) frame)
  (setf (slot-value message 'contact) (get-value-from-message frame)))



# Device Communication Plugin Manual

###### \[in package DEVICE-COMMUNICATION\]
[![pipeline status](https://gitlab.com/marvin-aiot/marvin-device/plugins/device-communication/badges/master/pipeline.svg)](https://gitlab.com/marvin-aiot/marvin-device/plugins/device-communication/commits/master)

## Description



## Installing device-communication

The first step it to copy the sources to your local-projects folder:

```bash
cd $HOME/quicklisp/local-projects
git clone https://gitlab.com/marvin-aiot/marvin-device/plugins/device-communication.git
```

After the files are copied, we can use [QuickLisp](https://www.quicklisp.org/beta/ "QuickLisp") to load device-communication:

```lisp
(ql:quickload :device-communication)
```


## License Information

This library is released under the GPL Version 3. Please refer to the [LICENSE](https://gitlab.com/marvin-aiot/marvin-device/plugins/device-communication/blob/master/LICENSE "License") to get the full licensing text.

## Contributing to this project

Please refer to the [CONTRIBUTING](https://gitlab.com/marvin-aiot/marvin-device/plugins/device-communication/blob/master/CONTRIBUTING.md "Contributing") document for more information.

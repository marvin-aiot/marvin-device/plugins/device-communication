;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


(in-package :device-communication)


(defclass heartbeat-queue ()
  ((queue :initform nil
          :type list
          :accessor queue)))

(defconstant +broadcast-device-id+ 0)
(defvar *self-ref* nil)
(defvar *device-ref* nil)
(defvar *device-message-ref* nil)
(defvar *device-service-ref* nil)
(defvar *subscriptions/messages* nil)
(defvar *subscriptions/discover* nil)


(defclass device (marvin:ding)
  ((device-id :initarg :device-id
              :reader device-id)
   (device-sn :initarg :device-sn
              :reader device-sn)))

(defclass device-message (marvin:ding)
  ((device-message-id :initarg :device-message-id
                      :reader device-message-id)))

(defclass device-service (marvin:ding)
  ((device-service-id :initarg :device-service-id
                      :reader device-service-id)))


;; ------------------------------- BEGIN Handling HTTP & WS routes -------------------------------

#|

End-points
http://host:port/api/v1/device-communication/devices -> list-devices

ws://host:port/api/v1/device-communication/messages {action=subscribe; message-id=123; device-id=12}
ws://host:port/api/v1/device-communication/messages {action=unsubscribe; message-id=123 ;device-id=12}


Handling subscriptions

At the moment only one resource is avaliable, /api/v1/device-communication/message
So *subscriptions/messages* can only contain one element, although many clients can subscribe to that resource.
An each client might subscribe to multiple device-id/message-id/uid triplets.

On client disconnect the element of (clients subscription) with (eq (user client) user) must be removed.
If (clients subscription) is afterward empty, the subscription must be removed from *subscriptions/messages*.

On client subscribe the new device-id/message-id/uid triplet is added to the client.
If no client for that user exists, a new one is pushed into (clients subscription).

On client unsubscribe the device-id/message-id/uid triplet is removed from (messages client).
If (messages client) becomes empty the client is removed from (clients subscription).

On receiving a new message from a Device every (clients subscription) that have a (messages client)
matching (and (eq device-id (device-id x)) (eq message-id (message-id x))) is notified.

|#

(defclass message ()
  ((device-id :initarg :device-id
              :reader device-id)
   (message-id :initarg :message-id
               :reader message-id)))

(defclass client ()
  ((user :initarg :user
         :reader user)))

(defclass message-client (client)
  ((messages :initarg :messages
             :accessor client-messages)))

(defclass subscription ()
  ((clients :initarg :clients
            :accessor clients)
   (resource :initarg :resource
             :reader resource)))

(defclass discover-subscription-json ()
  ((action :initarg :action :reader action :type string)))

(c2mop:ensure-finalized (find-class 'discover-subscription-json))

(defun discover-client-disconnected (resource user)
  ;; Remove all subscriptions of that user
  (log:info "User disconnected: " user)
  (let ((subscription (car (member :messages *subscriptions/discover* :key #'(lambda (x) (end-point:resource-name (resource x)))))))
    (when subscription
      (unless (eq resource (resource subscription))
        (break "The resources do not match!"))

      (trivial-object-lock:with-object-lock-held (*subscriptions/discover* :test #'eq)
        (let ((subscription (car (member resource *subscriptions/discover* :key #'resource))))
          (when (member user (clients subscription) :key #'user)
            (setf (clients subscription) (delete user (clients subscription) :key #'user))
            (unless (clients subscription)
              (setf *subscriptions/discover* (delete subscription *subscriptions/discover*)))))))))

(defun handle-discover-ws-endpoint (resource user message)
  ;; Actions
  ;; trigger-discovery: send an RTR 252 frame to broadcast ID
  ;; commission: provides the Device with a new ID and adds it as a known Device to the DB
  ;; decommission: @TODO How to handle Rules and other stuff which might reference the Device?
  (let ((json (trivial-json-codec:deserialize-json message :class (find-class 'discover-subscription-json))))
    (unless (action json)
      (log:error "Parsing error '~a'." message)
      (return-from handle-discover-ws-endpoint))

    (cond ((string-equal (action json) "subscribe")
           (subscribe-discover-ep resource user json))
          ((string-equal (action json) "unsubscribe")
           (unsubscribe-discover-ep user json))
          ((string-equal (action json) "trigger-discovery")
           )
          ((string-equal (action json) "commission")
           ;; Payload contains: 1. SerialNumber 2. Device-ID
           ;; If no DB obj is found by Serialnumber, a new one is created
           ;;   Otherwise (setf (marvin:eigenschaft device-obj :disabled) nil)
           ;; Unless (marvin:eigenschaft device-obj :device-id) changed or new device obj was created
           ;;   initiate ISS
           )
          ((string-equal (action json) "decommission")
           ;; (setf (marvin:eigenschaft device-obj :disabled) t)
           )
          (t (log:error "Invalid action '~a'" (action json))))))

(defun subscribe-discover-ep (resource user json)
  (declare (ignore json))
  (trivial-object-lock:with-object-lock-held (*subscriptions/discover* :test #'eq)
    (let ((subscription (car (member :messages *subscriptions/discover* :key #'(lambda (x) (end-point:resource-name (resource x)))))))
      (if subscription
          (trivial-utilities:aif (car (member user (clients subscription) :key #'user))
                                 (log:error "User ~a is already subscribed." user)
                                 (push (make-instance 'client :user user)
                                       (clients subscription)))

          (push (make-instance 'subscription
                               :resource resource
                               :clients (list (make-instance 'client :user user)))
                *subscriptions/discover*)))))

(defun unsubscribe-discover-ep (user json)
  (declare (ignore json))
  (trivial-object-lock:with-object-lock-held (*subscriptions/discover* :test #'eq)
    (let ((subscription (car (member :messages *subscriptions/discover* :key #'(lambda (x) (end-point:resource-name (resource x)))))))
      (if subscription
          (trivial-utilities:aif (car (member user (clients subscription) :key #'user))
                                 (unless (setf (clients subscription)
                                               (delete user (clients subscription) :key #'user))
                                   (setf *subscriptions/discover*
                                         (delete :messages *subscriptions/discover* :key #'(lambda (x) (end-point:resource-name (resource x))))))
                                 (log:error "User ~a was not subscribed." user))
          (log:error "No Subscription for :messages was found.")))))

;; ------------------------------- END Handling HTTP & WS routes -------------------------------


;; @TODO Create filter method to handle :send-message

(defun set-object-online-status (id status)
  (declare (type (unsigned-byte 8) id))

  (let ((obj (marvin:find-ding `(:and (:eigenschaft :?_ :klasse ,*device-ref*)
                                      (:eigenschaft :?_ :device-id ,id)))))
    (unless obj
      (log:warn "Could not find Device with Device-Id ~a to update its status." id)
      (return-from set-object-online-status))

      (unless (eq (marvin:eigenschaft obj :device-status) status)
        (log:info "The Device ~a (Id ~a) just went ~a."
                  (marvin:eigenschaft obj :device-id)
                  id
                  (if (eq status :offline) "offline" "online"))
        (setf (marvin:eigenschaft obj :device-status) status))))

(defun create-error-frame (error-type source-id)
  (log:error "An error has occured! ~a ~a" error-type source-id))

(defun get-value-key-from-message (message)
  (case (communication-protocol:message-id message)
          (31  'communication-protocol:ttl)
          (208 'communication-protocol:offset)
          (230 'movement)
          (252 'communication-protocol:serial-number)
          (547 'status)
          (560 'temperature)
          (561 'humidity)
          (562 'luminosity)
          (563 'air-pressure)
          (564 'air-dust)
          (565 'air-quality)
          (580 'contact)))

(defun get-value-from-message (message)
  (let ((value nil)
        (key (get-value-key-from-message message)))
    (setf value (slot-value message key))
    (case (communication-protocol:message-id message)
      (560 ;; Temperature
       (setf value (* (coerce (the fixnum (- (the fixnum value) 27300)) 'float) 0.01)))
      (561 ;; Humidity
       (setf value (* (coerce value 'float) 0.0015267176))))
    (return-from get-value-from-message value)))

(defun make-frame-from-message (message layer)
  "Creates a new Frame based on the information given by the message"
  (declare (type communication-protocol:message message)
           (ignore layer))

  (when (eq (communication-protocol:source-id message) (marvin:eigenschaft *self-ref* :device-id))
    (log:debug "Ignoring self-sent message.")
    (return-from make-frame-from-message))

  (let ((device-obj (marvin:find-ding `(:and (:eigenschaft :?_ :klasse ,*device-ref*)
                                             (:eigenschaft :?_ :device-id ,(communication-protocol:source-id message)))))
        (message-obj (marvin:find-ding `(:and (:eigenschaft :?_ :klasse ,*device-message-ref*)
                                              (:eigenschaft :?_ :device-message-id ,(communication-protocol:message-id message))))))

    (unless message-obj
      (create-error-frame :unknown-message (list :device-id (communication-protocol:source-id message) :message-id (communication-protocol:message-id message)))
      (return-from make-frame-from-message))

    (unless device-obj
      (if (eq +broadcast-device-id+ (communication-protocol:source-id message))
          (progn
            ;; @REVIEW If there are subscribers to :discover and the message-id is 252 then handling of 'new' devices should be possible.
            ;; When (null device-obj) the commission-device is possible, otherwise decommission-device
            (create-error-frame :device-not-commissioned (list :device-id (communication-protocol:source-id message) :message-id (communication-protocol:message-id message))))
          (create-error-frame :unknown-device (list :device-id (communication-protocol:source-id message) :message-id (communication-protocol:message-id message))))
      (return-from make-frame-from-message))

    (when (marvin:eigenschaft device-obj :disabled)
      (return-from make-frame-from-message))

    ;;(let ((Marvin.Cognitron:*learning-rate* 0.0))
    (let ((ding (marvin:create-object (list
                                       :klasse (make-instance 'marvin:ding-reference
                                                              :id (marvin:get-id-for-class
                                                                   (find-class 'marvin:frame))) ;; @TODO Could we use an own ID?
                                                                                                                        ;; @REVIEW What about using (marvin:id message-obj) ??
                                       :p-list (list
                                                :device device-obj
                                                :message message-obj
                                                :counter (communication-protocol:message-counter message)
                                                :value (get-value-from-message message)
                                                :timestamp (get-universal-time)
                                                :granularity :instant
                                                :action :cc_in
                                                :actor *self-ref*)))))

      ;; Update the related K-Line for this device/message pair
      (let ((message-kline (cadr (assoc message-obj (marvin:eigenschaft device-obj :message-klines)))))
        (when message-kline
          (consolidate-message-kline message-kline ding))))))

;; ------------------------- BEGIN Consolidation of sensor K-Line data -------------------------

(let ((granularities '(:instant :hour :day :month :year)))
  (defun next-higher-granularity (granularity)
    (declare (type (member :hour :day :month :year) granularity))
    (if (eq :year granularity)
        :year
        (nth (1+ (position granularity granularities)) granularities)))

  (defun next-lower-granularity (granularity)
    (declare (type (member :hour :day :month :year) granularity))
    (if (eq :instant granularity)
        :instant
        (nth (1- (position granularity granularities)) granularities))))

(defun calculate-timestamp-for-internal-grouping (frame granularity)
  (multiple-value-bind (second minute hour date month year day daylight-p zone)
      (decode-universal-time (ding:erstellungsdatum frame))
    (declare (ignore minute second day daylight-p zone))
    (let ((ts (local-time:encode-timestamp 0 0 0 hour date month year)))
      (ecase granularity
        (:hour (list :to (1- (local-time:timestamp-to-universal
                              (local-time:adjust-timestamp ts
                                (offset :hour +1))))
                     :from (encode-universal-time 0 0 hour date month year)))
        (:day (list :to (1- (local-time:timestamp-to-universal
                             (local-time:adjust-timestamp ts
                               (set :hour 0)
                               (offset :day +1))))
                    :from (encode-universal-time 0 0 0 date month year)))
        (:month (list :to (1- (local-time:timestamp-to-universal
                               (local-time:adjust-timestamp ts
                                 (set :hour 0)
                                 (set :day 1)
                                 (offset :month +1))))
                      :from (encode-universal-time 0 0 0 1 month year)))
        (:year (list :to (1- (local-time:timestamp-to-universal
                              (local-time:adjust-timestamp ts
                                (set :hour 0)
                                (set :day 1)
                                (set :month 1)
                                (offset :year +1))))
                     :from (encode-universal-time 0 0 0 1 1 year)))))))

(defun calculate-timestamp-for-grouping (frame granularity)
  (multiple-value-bind (second minute hour date month year day daylight-p zone)
      (decode-universal-time (ding:erstellungsdatum frame))
    (declare (ignore minute second day daylight-p zone))
    (let ((ts (local-time:encode-timestamp 0 0 0 hour date month year)))
      (ecase granularity
        (:hour (list :from (local-time:timestamp-to-universal
                            (local-time:adjust-timestamp ts
                             (offset :hour -1)))
                     :to (1- (encode-universal-time 0 0 hour date month year))))
        (:day (list :from (local-time:timestamp-to-universal
                            (local-time:adjust-timestamp ts
                              (set :hour 0)
                              (offset :day -1)))
                    :to (1- (encode-universal-time 0 0 0 date month year))))
        (:month (list :from (local-time:timestamp-to-universal
                            (local-time:adjust-timestamp ts
                              (set :hour 0)
                              (set :day 1)
                              (offset :month -1)))
                      :to (1- (encode-universal-time 0 0 0 1 month year))))
        (:year (list :from (local-time:timestamp-to-universal
                            (local-time:adjust-timestamp ts
                              (set :hour 0)
                              (set :day 1)
                              (set :month 1)
                              (offset :year -1)))
                     :to (1- (encode-universal-time 0 0 0 1 1 year))))))))

(defun find-grouping-by-granularity (frames granularity)
  (let ((timeinterval (calculate-timestamp-for-grouping (car frames) granularity))
        (frame-grain (next-lower-granularity granularity))
        (result nil)
        (collector nil))
    (iterate:iterate
      (iterate:with ts-from = (getf timeinterval :from))
      (iterate:with ts-to = (getf timeinterval :to))
      (iterate:for frame in (cdr frames))
      (iterate:for frame-ts-from = (or (marvin:eigenschaft frame :timestamp)
                                  (marvin:eigenschaft frame :timestamp-from)))
      (iterate:for frame-ts-to = (or (marvin:eigenschaft frame :timestamp)
                                  (marvin:eigenschaft frame :timestamp-to)))
      ;;(iterate:while (eq frame-grain (marvin:eigenschaft frame :granularity)))
      (when (and (eq (marvin:eigenschaft frame :granularity) frame-grain)
                 (<= frame-ts-from ts-to)) ;; We skip all frames from the same time slot
        ;;(log:info "Considering ~a" frame)
        (if (>= frame-ts-to ts-from)
            (push frame collector)
            (progn
              (push (list (reverse collector) ts-from ts-to) result)
              (setf collector (list frame))
              (let ((new-timeinterval (calculate-timestamp-for-internal-grouping frame granularity)))
                (setf ts-from (getf new-timeinterval :from))
                (setf ts-to (getf new-timeinterval :to))))))
      (iterate:finally (when collector (push (list (reverse collector) ts-from ts-to) result))))
    (reverse result)))

(defun create-statistics (frames)
  (declare (type cons frames))
  (iterate:iterate
    (iterate:for frame in frames)
    (iterate:for value = (marvin:eigenschaft frame :value))
    (iterate:counting frame into counter)
    (iterate:sum value into sum)
    (iterate:maximize value into maximum)
    (iterate:minimize value into minimum)
    (iterate:finally (return (list counter maximum minimum (/ sum counter) nil nil)))))

(defun consolidate-statistics (frames)
  (declare (type cons frames))
  ;; Similar to create-statistics, but based on groupings
  (iterate:iterate
    (iterate:for frame in frames)
    (iterate:sum (marvin:eigenschaft frame :count) into counter)
    (iterate:sum (* (marvin:eigenschaft frame :count)
                    (marvin:eigenschaft frame :mean))
                 into sum)
    (iterate:maximize (marvin:eigenschaft frame :maximum) into maximum)
    (iterate:minimize (marvin:eigenschaft frame :minimum) into minimum)
    (iterate:finally (return (list counter maximum minimum (/ sum counter) nil nil)))))

(defun consolidate-frames (k-line granularity)
  (let ((groups (find-grouping-by-granularity (k-line:child-frames k-line)
                                              granularity)))
    (when groups
      (iterate:iterate
        (iterate:for (grouped-frames ts-from ts-to) in groups)
        (if grouped-frames
            (destructuring-bind (counter maximum minimum mean median tendency)
                (if (eq :hour granularity)
                    (create-statistics grouped-frames)
                    (consolidate-statistics grouped-frames))

              (let ((grouping (marvin:create-object
                               (list
                                :klasse (make-instance 'marvin:ding-reference
                                                       :id (marvin:get-id-for-class
                                                            (find-class 'marvin:frame)))
                                :p-list (list :timestamp-from ts-from
                                              :timestamp-to ts-to
                                              :granularity granularity
                                              :count counter
                                              :maximum maximum
                                              :minimum minimum
                                              :mean mean
                                              :median median
                                              :tendency tendency
                                              :data grouped-frames)))))

                (k-line:replace-frames-from-kline k-line grouped-frames grouping)))
            (progn
              ;; This could happen i.e. when resuming operation after a hour offline
              ;; @TODO Give the log line better information
              (log:warn "No Frames could be collected!"))))

      (unless  (eq :year granularity)
        (log:info "Looking for consolidation after ~a" granularity)
        (consolidate-frames k-line (next-higher-granularity granularity))))))

(defun consolidate-message-kline (k-line new-frame)
  ;; All messages in the K-Line get grouped over time
  ;; Granularity is (:hour :day :month :year)
  ;; Groupings have following fields:
  ;;          granularity
  ;;          minimum
  ;;          maximum
  ;;          mean
  ;;          median - needs sorting
  ;;          range - (maximum - minimum)
  ;;          tendency - how?
  ;;          count
  ;;          data

  ;; When the timestamp of new-frame and the latest frame of the k-line
  ;; differ on any given granularity, consolidation avalanche is triggered

  ;; E.g. the new frame is from 13:00 and the previous one is from 12:59
  ;; Every frame that was created between 12:00 and 12:59 is collected and
  ;; a grouping is created. All frames are replaced by the later.
  ;; This triggers the consolidation of the next granularity level.
  (k-line:add-frame-to-kline new-frame k-line)
  (consolidate-frames k-line :hour)

  ;; @TODO Update (k-line:result-frame k-line)
  )

;; -------------------------- END Consolidation of sensor K-Line data --------------------------


(plugin-base:define-plugin "Device-Communication-Plugin"
    ((communication-interface :type (or null communication-protocol:communication-interface)
                              :initform nil
                              :reader communication-interface))
  (:thread-sleep-duration 0.1f0 :thread-max-restarts 3)
  ((:install-plugin (install-plugin this))
   (:initialize-plugin (initialize-plugin))
   (:start-plugin (start-plugin plugin))
   (:analyze (analyze plugin ding))
   (:stop-plugin (stop-plugin plugin))))

(defun start-plugin (plugin)
  (marvin:add-http-end-point :devices
                             "List all configured Devices."
                             :v1
                             :get
                             #'load-devices
                             :parents '(:device-communication))

  (marvin:add-ws-end-point :discover
                           "Discover Device (existing and new ones)."
                           :v1
                           :get
                           #'handle-discover-ws-endpoint
                           :parents '(:device-communication)
                           :on-disconnect #'discover-client-disconnected)
  
  (let ((config '(:routing-table ((:any :any :any "Client Layer")
                                  (:any :any "Client Layer" :known-to-layer))
                  :network-layers ((:type :client :name "Client Layer")
                                   (:type :udp :name "UDP Layer" :port 4242)
                                   (:type :web-socket :name "WebSocket Layer" :port 8082)))))

    (setf (slot-value plugin 'communication-interface)
          (communication-protocol:make-communication-interface config
                                           :on-event-fn #'make-frame-from-message
                                           :on-service-request-fn #'communication-protocol:default-service-request-fn
                                           :on-device-status-changed-fn #'set-object-online-status)))

  (communication-protocol:start (communication-interface plugin)))

(defun stop-plugin (plugin)
  (communication-protocol:stop (communication-interface plugin))

  (marvin:remove-http-end-point :devices :v1 :get '(:device-communication))
  (marvin:remove-ws-end-point :discover :v1 :get '(:device-communication)))

(defun analyze (plugin ding)
  (log:debug "I was called to analyze ~a." ding)

  ;; @TODO Decide whether a message should be sent to some Device

  (return-from analyze
    (make-instance 'marvin:Plugin-Response
                   :plugin plugin
                   :cost 1.0f0
                   :fit 1.0f0
                   :resolution :not-possible
                   :args nil)))

(defun initialize-plugin ()
  (let* ((name-field (marvin:find-ding `(:eigenschaft :?_ :graphie "Name")))
         (wort-klasse (marvin:find-ding `(:and (:eigenschaft :?1 :graphie "Wort")
                                               (:eigenschaft :?_ ,name-field  :?1)))))

    (unless (and name-field wort-klasse)
      (error "Could not load the Ding representing the word 'Name'.")) ;; @TODO

    (setf *self-ref* (marvin:find-ding `(:and (:eigenschaft :?name :klasse ,wort-klasse)
                                              (:eigenschaft :?name :graphie "Marvin")
                                              (:eigenschaft :?_ ,name-field :?name))))

    (ding:register-ding #'(lambda ()
                            (trivial-utilities:aif (marvin:find-ding `(:and (:eigenschaft :?name :klasse ,wort-klasse)
                                                                            (:eigenschaft :?name :graphie "Device")
                                                                            (:eigenschaft :?_ ,name-field :?name)))
                                                   (progn
                                                     (setf *device-ref* (make-instance 'ding:ding-reference :id (ding:id it)))
                                                     (values (ding:id it) (find-class 'device-communication:device)))
                                                   (error "No class for 'Device' found."))))

    (ding:register-ding #'(lambda ()
                            (trivial-utilities:aif (marvin:find-ding `(:and (:eigenschaft :?name :klasse ,wort-klasse)
                                                                            (:eigenschaft :?name :graphie "Device-Message")
                                                                            (:eigenschaft :?_ ,name-field :?name)))
                                                   (progn
                                                     (setf *device-message-ref* (make-instance 'ding:ding-reference :id (ding:id it)))
                                                     (values (ding:id it) (find-class 'device-communication:device-message)))
                                                   (error "No class for 'Device-Message' found."))))

    (ding:register-ding #'(lambda ()
                            (trivial-utilities:aif (marvin:find-ding `(:and (:eigenschaft :?name :klasse ,wort-klasse)
                                                                            (:eigenschaft :?name :graphie "Device-Service")
                                                                            (:eigenschaft :?_ ,name-field :?name)))
                                                   (progn
                                                     (setf *device-service-ref* (make-instance 'ding:ding-reference :id (ding:id it)))
                                                     (values (ding:id it) (find-class 'device-communication:device-service)))
                                                   (error "No class for 'Device-Service' found."))))))

(defun compile-expr (expr)
  (declare (type cons expr))
  (let ((search-params (iterate:iterate
                         (iterate:for (eigenschaft value) on expr by #'cddr)
                         (iterate:collect (list :eigenschaft :?_ eigenschaft value)))))
    (return-from compile-expr
      (if (> (length search-params) 1)
          (append '(:and) search-params)
          (car search-params)))))

(defun create-if-inexsitent (plist)
  ""
  (trivial-utilities:aif (marvin:find-ding (compile-expr plist))
                         it
                         (marvin:create-object (marvin:separate-by-slots plist))))

(defun install-plugin (this)
  (let* ((name-field (marvin:find-ding '(:eigenschaft :?_ :graphie "Name")))
         (wort-klasse (marvin:find-ding `(:and (:eigenschaft :?1 :graphie "Wort")
                                               (:eigenschaft :?_ ,name-field  :?1))))

         (unit-field (marvin:find-ding `(:and (:eigenschaft :?_ :klasse ,wort-klasse)
                                              (:eigenschaft :?_ :graphie "Einheit"))))
         (device-str (create-if-inexsitent (list :graphie "Device" :klasse wort-klasse)))
         (device-msg-str (create-if-inexsitent (list :graphie "Device-Message" :klasse wort-klasse)))
         (device-srv-str (create-if-inexsitent (list :graphie "Device-Service" :klasse wort-klasse)))
         (device (create-if-inexsitent (list name-field device-str)))
         (device-message (create-if-inexsitent (list name-field device-msg-str)))
         (device-service (create-if-inexsitent (list name-field device-srv-str)))
         (objects (list
                   (list :device-message-id 0 :klasse device-message)
                   (list :device-message-id 1 :klasse device-message)
                   (list :device-message-id 10 :klasse device-message)
                   (list :device-message-id 11 :klasse device-message)
                   (list :device-message-id 31 :klasse device-message)
                   (list :device-message-id 32 :klasse device-message)
                   (list :device-message-id 208 :klasse device-message)
                   (list :device-message-id 209 :klasse device-message)
                   (list :device-message-id 210 :klasse device-message)
                   (list :device-message-id 220 :klasse device-message name-field "CO2")
                   (list :device-message-id 221 :klasse device-message name-field "Rauch")
                   (list :device-message-id 230 :klasse device-message name-field "Bewegung")
                   (list :device-message-id 240 :klasse device-message name-field "Schliesskontakt")
                   (list :device-message-id 252 :klasse device-message)
                   (list :device-message-id 540 :klasse device-message name-field "Spannung" unit-field "V")
                   (list :device-message-id 541 :klasse device-message name-field "Strom" unit-field "A")
                   (list :device-message-id 542 :klasse device-message name-field "Leistung" unit-field "W")
                   (list :device-message-id 543 :klasse device-message name-field "Komplexe Leistung" unit-field "VA")
                   (list :device-message-id 544 :klasse device-message name-field "Scheinleistung" unit-field "W")
                   (list :device-message-id 545 :klasse device-message name-field "Leistungsfaktor" unit-field "%")
                   (list :device-message-id 546 :klasse device-message name-field "Sinusverzerrung")
                   (list :device-message-id 547 :klasse device-message name-field "Relais")
                   (list :device-message-id 560 :klasse device-message name-field "Temperatur" unit-field "°C")
                   (list :device-message-id 561 :klasse device-message name-field "Feuchtigkeit" unit-field "%RH")
                   (list :device-message-id 562 :klasse device-message name-field "Helligkeit")
                   (list :device-message-id 563 :klasse device-message name-field "Luftdruck")
                   (list :device-message-id 564 :klasse device-message name-field "Luftstaubkonzentration")
                   (list :device-message-id 565 :klasse device-message name-field "Luftqualität")
                   (list :device-message-id 566 :klasse device-message name-field "Windgeschwindigkeit" unit-field "m/s")
                   (list :device-message-id 567 :klasse device-message name-field "Windrichtung" unit-field "°")
                   (list :device-message-id 568 :klasse device-message name-field "Regenintensität" unit-field "mm/h")
                   (list :device-message-id 580 :klasse device-message name-field "Zustand")
                   (list :device-message-id 600 :klasse device-message name-field "Stromquelle")
                   (list :device-message-id 610 :klasse device-message name-field "Intensität" unit-field "%")
                   (list :device-message-id 611 :klasse device-message name-field "Intensität Rot" unit-field "%")
                   (list :device-message-id 612 :klasse device-message name-field "Intensität Grün" unit-field "%")
                   (list :device-message-id 613 :klasse device-message name-field "Intensität Blau" unit-field "%")
                   (list :device-message-id 620 :klasse device-message name-field "Batteriestatus")
                   (list :device-message-id 621 :klasse device-message name-field "Batterieladung" unit-field "%")
                   (list :device-message-id 700 :klasse device-message)
                   (list :device-message-id 701 :klasse device-message name-field "Lautstärke")
                   (list :device-message-id 710 :klasse device-message name-field "IR Befehl")
                   (list :device-message-id 870 :klasse device-message name-field "Laufzeit")

                   (list :device-service-id 0 :klasse device-service name-field "Device Information Service")
                   (list :device-service-id 1 :klasse device-service name-field "Data Download Service")
                   (list :device-service-id 2 :klasse device-service name-field "Data Upload Service")
                   (list :device-service-id 3 :klasse device-service name-field "Device-Id setting Service")
                   (list :device-service-id 4 :klasse device-service name-field "Parameter Information Service"))))

    (ding:register-ding #'(lambda ()
                            (values (ding:id device)
                                    (find-class 'device-communication:device))))

    (ding:register-ding #'(lambda ()
                            (values (ding:id device-message)
                                    (find-class 'device-communication:device-message))))

    (ding:register-ding #'(lambda ()
                            (values (ding:id device-service)
                                    (find-class 'device-communication:device-service))))

    (pushnew (list device 1.0f0) (marvin:eigenschaft this :associations) :test #'(lambda (x y) (trivial-utilities:equals (car x) (car y))))
    (pushnew (list device-message 1.0f0) (marvin:eigenschaft this :associations) :test #'(lambda (x y) (trivial-utilities:equals (car x) (car y))))
    (pushnew (list device-service 1.0f0) (marvin:eigenschaft this :associations) :test #'(lambda (x y) (trivial-utilities:equals (car x) (car y))))

    ;; Push device-communication-plugin to Marvin's associations
    (trivial-utilities:aif (marvin:find-ding `(:and (:eigenschaft :?name :graphie "Marvin")
                                                    (:eigenschaft :?_ ,name-field :?name)))
                           (pushnew (list this 1.0f0) (marvin:eigenschaft it :associations) :test #'(lambda (x y) (trivial-utilities:equals (car x) (car y))))
                           (log:error "Could not load the 'Marvin' Ding. No associations could be created; this will have a negative impact on this Agent."))

    (map 'null #'create-if-inexsitent objects))

  #+non-persistent-working-memory
  (init-devices))

(defun load-devices (resource request)
  (declare (ignore resource request))
  (log:info "Loading known devices...")
  (marvin:with-eigenschaft-cache
      (let* ((name-field (marvin:find-ding `(:eigenschaft :?_ :graphie "Name")))
             (wort-klasse (marvin:find-ding `(:and (:eigenschaft :?1 :graphie "Wort")
                                                   (:eigenschaft :?_ ,name-field  :?1))))
             (device-class (marvin:find-ding `(:and (:eigenschaft :?name :klasse ,wort-klasse)
                                                    (:eigenschaft :?name :graphie "Device")
                                                    (:eigenschaft :?_ ,name-field :?name))))
             (device-message-class (marvin:find-ding `(:and (:eigenschaft :?name :klasse ,wort-klasse)
                                                            (:eigenschaft :?name :graphie "Device-Message")
                                                            (:eigenschaft :?_ ,name-field :?name)))))

        (assert device-class)
        (assert device-message-class)

        (let ((devices (marvin:find-all-dings `(:and (:eigenschaft :?_ :klasse ,device-class))))
              (feld-beschreibung (marvin:find-ding `(:and (:eigenschaft :?_ :klasse ,wort-klasse)
                                                          (:eigenschaft :?_ :graphie "Beschreibung"))))
              (feld-einheit (marvin:find-ding `(:and (:eigenschaft :?_ :klasse ,wort-klasse)
                                                     (:eigenschaft :?_ :graphie "Einheit"))))
              (placeholder (make-instance 'trivial-variable-bindings:place-holder :name "_")))

          (with-output-to-string (stream)
            (princ #\[ stream)
            (iterate:iterate
             (iterate:for device
                          initially (trivial-continuation:result devices)
                          then (trivial-continuation:result
                                (trivial-continuation:cc/continue devices)))
             (iterate:while device)
             (progn
               (unless (iterate:first-time-p)
                 (princ #\, stream))
               (create-device-block stream (trivial-variable-bindings:bound-variable-value placeholder device)
                                    device-message-class name-field feld-beschreibung feld-einheit)))
            (princ #\] stream))))))

(defun create-device-block (stream device message-class feld-name feld-beschreibung feld-einheit)
  (declare (type stream stream)
           (type marvin:ding-base device message-class feld-name feld-beschreibung feld-einheit))

  (format stream
          "
          {
             \"id\":~a,
             \"name\":\"~a\",
             \"type\":\"~a\",
             \"type_id\":1,
             \"description\":\"~a\",
             \"slots\":
             ["
          (marvin:eigenschaft device :device-id :ignore-inheritance t)
          (let ((val (marvin:eigenschaft device feld-name :ignore-inheritance t)))
            (if (typep val 'ding:ding-base)
                (marvin:eigenschaft val :graphie)
                val))
          (marvin:eigenschaft device :type :ignore-inheritance t)
          ;;(marvin:eigenschaft device :type-id :ignore-inheritance t)
          (let ((val (marvin:eigenschaft device feld-beschreibung :ignore-inheritance t)))
            (if (typep val 'ding:ding-base)
                (marvin:eigenschaft val :graphie)
                val)))

  (iterate:iterate
    (iterate:for message in (marvin:eigenschaft device message-class))
    (let ((msg-ding (if (typep message 'marvin:ding-reference)
                        (marvin:find-object-by-id (marvin:id message))
                        message)))
      (unless (iterate:first-time-p)
        (princ #\, stream))
      (create-message-block stream msg-ding feld-name feld-beschreibung feld-einheit)))
  (format stream "
             ]
          }"))

(defun create-message-block (stream message feld-name feld-beschreibung feld-einheit)
  (declare (type stream stream)
           (type marvin:ding-base message feld-name feld-beschreibung feld-einheit))
  (format stream
          "
              {
                 \"message_id\":~a,
                 \"slot_name\":\"~a\",
                 \"slot_description\":\"~a\",
                 \"slot_unit\":\"~a\",
                 \"data_type\":\"~a\"
              }"
          (marvin:eigenschaft message :device-message-id :ignore-inheritance t)
          (let ((val (marvin:eigenschaft message feld-name :ignore-inheritance t)))
            (if (typep val 'ding:ding-base)
                (marvin:eigenschaft val :graphie)
                val))
          (let ((val (marvin:eigenschaft message feld-beschreibung :ignore-inheritance t)))
            (if (typep val 'ding:ding-base)
                (marvin:eigenschaft val :graphie)
                val))
          (let ((val (marvin:eigenschaft message feld-einheit :ignore-inheritance t)))
            (if (typep val 'ding:ding-base)
                (marvin:eigenschaft val :graphie)
                val))
          (marvin:eigenschaft message :daten-typ :ignore-inheritance t)))

#+non-persistent-working-memory
(defun init-devices ()
  (log:info "Initializing Devices")
  ;; @TODO Check whether Plugin device-communication is enabled and installed
  (unless (find-class 'device-communication:device nil)
    (log:info "Plugin device-communication is nor enabled or not installed.")
    (return-from init-devices))

  (let* ((name-field (marvin:find-ding '(:eigenschaft :?_ :graphie "Name")))
         (wort-klasse (marvin:find-ding `(:and (:eigenschaft :?1 :graphie "Wort")
                                                       (:eigenschaft :?_ ,name-field  :?1))))
         (description-field (marvin:find-ding `(:and (:eigenschaft :?_ :klasse ,wort-klasse)
                                                      (:eigenschaft :?_ :graphie "Beschreibung"))))
         (device-str (marvin:find-ding `(:and (:eigenschaft :?_ :klasse ,wort-klasse)
                                                      (:eigenschaft :?_ :graphie "Device"))))
         (device-msg-str (marvin:find-ding `(:and (:eigenschaft :?_ :klasse ,wort-klasse)
                                                          (:eigenschaft :?_ :graphie "Device-Message"))))
         (device-message (marvin:find-ding `(:eigenschaft :?_ ,name-field ,device-msg-str)))
         (device (marvin:find-ding `(:eigenschaft :?_ ,name-field ,device-str)))
         (kline-str (marvin:find-ding `(:and (:eigenschaft :?_ :klasse ,wort-klasse)
                                                      (:eigenschaft :?_ :graphie "K-Line"))))
         (k-line (marvin:find-ding `(:eigenschaft :?_ ,name-field ,kline-str)))
         (msg-230 (marvin:find-ding `(:and (:eigenschaft :?_ :klasse ,device-message)
                                                   (:eigenschaft :?_ :device-message-id 230))))
         (msg-560 (marvin:find-ding `(:and (:eigenschaft :?_ :klasse ,device-message)
                                                   (:eigenschaft :?_ :device-message-id 560))))
         (msg-561 (marvin:find-ding `(:and (:eigenschaft :?_ :klasse ,device-message)
                                                   (:eigenschaft :?_ :device-message-id 561))))
         (msg-562 (marvin:find-ding `(:and (:eigenschaft :?_ :klasse ,device-message)
                                                   (:eigenschaft :?_ :device-message-id 562))))
         (msg-563 (marvin:find-ding `(:and (:eigenschaft :?_ :klasse ,device-message)
                                                   (:eigenschaft :?_ :device-message-id 563))))
         (msg-564 (marvin:find-ding `(:and (:eigenschaft :?_ :klasse ,device-message)
                                                   (:eigenschaft :?_ :device-message-id 564))))
         (msg-580 (marvin:find-ding `(:and (:eigenschaft :?_ :klasse ,device-message)
                                                   (:eigenschaft :?_ :device-message-id 580))))
         (msg-547 (marvin:find-ding `(:and (:eigenschaft :?_ :klasse ,device-message)
                                                   (:eigenschaft :?_ :device-message-id 547)))))

    (marvin:create-object
     (marvin:separate-by-slots
      (list :klasse device :device-sn 3036884 :device-id 13 name-field "DEV013"
            description-field "Sensor Schlaffzimmer"
            device-message (list msg-230 msg-560 msg-561 msg-562)
            :message-klines (list (list msg-230 (marvin:create-object (marvin:separate-by-slots (list :klasse k-line))))
                                  (list msg-560 (marvin:create-object (marvin:separate-by-slots (list :klasse k-line))))
                                  (list msg-561 (marvin:create-object (marvin:separate-by-slots (list :klasse k-line))))
                                  (list msg-562 (marvin:create-object (marvin:separate-by-slots (list :klasse k-line))))))))

    (marvin:create-object
     (marvin:separate-by-slots
      (list :klasse device :device-sn 3036409 :device-id 14 name-field "DEV014"
            description-field "Sensor Badezimmer" device-message (list msg-230 msg-560 msg-561 msg-562)
            :message-klines (list (list msg-230 (marvin:create-object (marvin:separate-by-slots (list :klasse k-line))))
                                  (list msg-560 (marvin:create-object (marvin:separate-by-slots (list :klasse k-line))))
                                  (list msg-561 (marvin:create-object (marvin:separate-by-slots (list :klasse k-line))))
                                  (list msg-562 (marvin:create-object (marvin:separate-by-slots (list :klasse k-line))))))))

    (marvin:create-object
     (marvin:separate-by-slots
      (list :klasse device :device-sn 3035091 :device-id 15 name-field "DEV015"
            description-field "Sensor Küche" device-message (list msg-230 msg-560 msg-561 msg-562)
            :message-klines (list (list msg-230 (marvin:create-object (marvin:separate-by-slots (list :klasse k-line))))
                                  (list msg-560 (marvin:create-object (marvin:separate-by-slots (list :klasse k-line))))
                                  (list msg-561 (marvin:create-object (marvin:separate-by-slots (list :klasse k-line))))
                                  (list msg-562 (marvin:create-object (marvin:separate-by-slots (list :klasse k-line))))))))

    (marvin:create-object
     (marvin:separate-by-slots
      (list :klasse device :device-sn 3034901 :device-id 16 name-field "DEV016"
            description-field "Sensor Wohnzimmer" device-message (list msg-230 msg-560 msg-561 msg-562)
            :message-klines (list (list msg-230 (marvin:create-object (marvin:separate-by-slots (list :klasse k-line))))
                                  (list msg-560 (marvin:create-object (marvin:separate-by-slots (list :klasse k-line))))
                                  (list msg-561 (marvin:create-object (marvin:separate-by-slots (list :klasse k-line))))
                                  (list msg-562 (marvin:create-object (marvin:separate-by-slots (list :klasse k-line))))))))

    (marvin:create-object
     (marvin:separate-by-slots
      (list :klasse device :device-sn 3037310 :device-id 17 name-field "DEV017"
            description-field "Sensor Hall" device-message (list msg-230 msg-560 msg-561 msg-562)
            :message-klines (list (list msg-230 (marvin:create-object (marvin:separate-by-slots (list :klasse k-line))))
                                  (list msg-560 (marvin:create-object (marvin:separate-by-slots (list :klasse k-line))))
                                  (list msg-561 (marvin:create-object (marvin:separate-by-slots (list :klasse k-line))))
                                  (list msg-562 (marvin:create-object (marvin:separate-by-slots (list :klasse k-line))))))))

    (marvin:create-object
     (marvin:separate-by-slots
      (list :klasse device :device-sn 0000000 :device-id 18 name-field "DEV018"
            description-field "Sensor Luftqualität" device-message (list msg-230 msg-560 msg-563 msg-564)
            :message-klines (list (list msg-230 (marvin:create-object (marvin:separate-by-slots (list :klasse k-line))))
                                  (list msg-560 (marvin:create-object (marvin:separate-by-slots (list :klasse k-line))))
                                  (list msg-563 (marvin:create-object (marvin:separate-by-slots (list :klasse k-line))))
                                  (list msg-564 (marvin:create-object (marvin:separate-by-slots (list :klasse k-line))))))))

    (marvin:create-object
     (marvin:separate-by-slots
      (list :klasse device :device-sn 13309637 :device-id 20 name-field "DEV020"
            description-field "Switch Küche" device-message (list msg-580 msg-547)
            :message-klines (list (list msg-547 (marvin:create-object (marvin:separate-by-slots (list :klasse k-line))))
                                  (list msg-580 (marvin:create-object (marvin:separate-by-slots (list :klasse k-line))))))))))


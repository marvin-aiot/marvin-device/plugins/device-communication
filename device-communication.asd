;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(defsystem :device-communication
  :name "device-communication"
  :description "Marvin Plugin Device Communication"
  :long-description ""
  :version "0.1.0"
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "GPL-3.0-or-later"
  :in-order-to ((test-op (test-op :device-communication/test)))
  :depends-on (:trivial-utilities
	       :trivial-object-lock
	       :trivial-json-codec
	       :communication-protocol
	       :marvin-base
	       :log4cl
	       :arnesi
	       :local-time
	       :usocket
	       :ip-interfaces)
  :components ((:file "package")
	       (:file "device-communication")
	       (:module "events"
		:components ((:file "event-230")
			     (:file "event-240")
			     (:file "event-547")
			     (:file "event-560")
			     (:file "event-561")
			     (:file "event-562")
			     (:file "event-580")))))

(defsystem :device-communication/test
  :name "device-communication/test"
  :description "Unit Tests for the device-communication project."
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "GPL-3.0-or-later"
  :depends-on (:device-communication
	       :fiveam)
  :perform (test-op (o s) (uiop:symbol-call :fiveam  '#:run! :device-communication-tests))
  :components ((:file "test-device-communication")))

